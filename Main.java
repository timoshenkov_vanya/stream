package stream;

import java.io.IOException;
import java.util.*;
import java.nio.charset.StandardCharsets;

import java.util.function.Function;
import java.util.stream.Collectors;


public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String[] strings = scanner.nextLine().split("\\s+");
        List<String> myList = new ArrayList<>(Arrays.asList(strings));


        myList = myList.stream().map(el -> {
            for (char c : el.toCharArray()) {
                if (!Character.isLetter(c)) {
                    return null;
                }
            }
            return el.toLowerCase();
        }).collect(Collectors.toList());
        myList.removeAll(Collections.singleton(null));
        Map<String, Long> myMapList = myList.stream().collect(Collectors.groupingBy(el -> el, Collectors.counting()));


        myMapList.entrySet().stream().sorted(Map.Entry.comparingByKey()).sorted(Comparator.
                        comparing((Function<Map.Entry<String, Long>, Long>) Map.Entry::getValue).
                reversed()).map(Map.Entry::getKey).limit(10).
                forEach(System.out::println);


    }
}
